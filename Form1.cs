using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;


namespace KPZ
{
    public partial class Form1 : Form
    {
        private string pythonexe = "";
        private Panel startpanel = new Panel();
        private PictureBox pictureBox1 = new PictureBox();
        private bool button1WasClicked = false;
        public int Count = 1;
        public int top = 10;
        public int left = 10;
        public int tempHeight;
        public Form1()
        {
            InitializeComponent();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Panel tempbut = new Panel();
            var ListOfPanels = this.Controls
                     .OfType<Panel>();
            base.OnPaint(e);
            foreach (Panel b in ListOfPanels)
            {
                if (b.Name != "button1" && b.Name != "button2")
                {
                    Point tempPointFirst = new Point();
                    Point tempPointSecond = new Point();
                    tempPointFirst.X = b.Location.X + 100;
                    tempPointFirst.Y = b.Location.Y + 100;
                    Pen cvet = new Pen(Color.Red);
                    cvet.Width = 10;
                    using (var blackPen = new Pen(Color.Black, 3))
                        e.Graphics.DrawLine(cvet, tempPointFirst, new Point(tempbut.Location.X + 100, tempbut.Location.Y + 100));
                    tempbut = b;
                }
            }
            //using (var blackPen = new Pen(Color.Black, 3))
            // e.Graphics.DrawLine(blackPen, button2.Location, button1.Location);
        }
        public void button1_Click(object sender, EventArgs e)
        {

            TextBox textbox = new TextBox();
            Button button = new Button();
            Panel panel = new Panel();

            Button button2 = new Button();
            panel.BackColor = Color.Black;
            textbox.Left = left;
            textbox.Top = top;
            button.Top = top;
            button.Left = left;
            button2.BackColor = Color.White;
            panel.Size = new Size(200, 110);
            panel.Location = new Point(left, top);
            button2.Left = 40;
            button2.Top = 20;
            button2.Text = textBox1.Text;
            button2.Width = 130;
            panel.Controls.Add(button2);
            //this.Controls.Add(textbox);
            //this.Controls.Add(button);
            this.Controls.Add(panel);
            button.Text = textBox1.Text;
            tempHeight = panel.Height;
            top += tempHeight + 20;
            Count++;
            this.Invalidate();
            var checkBoxes = this.Controls
                     .OfType<Button>()
                     .TakeWhile<Button>(cb => cb.Visible);
            textBox1.Text = "";
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Controls.RemoveAt(Count + 4);
            Count--;
            top = top - tempHeight - 20;
            this.Invalidate();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            StreamWriter writer = new StreamWriter(@"text.txt");
            List<string> listofstring = new List<string>();
            var text = this.Controls;
            var ListOfPanels = this.Controls
         .OfType<Panel>();
            int i = 0;
            int j = 0;
            foreach (Panel p in ListOfPanels)
            {
                text = p.Controls;
                foreach (Button b in text)
                {
                    if (i == j)
                    {
                        listofstring.Add(b.Text);
                    }
                    j++;
                }
                i++;
            }


            using (writer)
            {
                foreach (string o in listofstring)
                {

                    //Console.WriteLine(o);

                    writer.WriteLineAsync(o);

                }
            }
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = textBox2.Text;//cmd is full path to python.exe
            start.Arguments = @"..\..\2221.py";//args is path to .py file and any cmd line args
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Console.Write(result);
                }

            }
            Process.Start(@"text.txt");
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
